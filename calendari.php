<?php
    $diaActual = date("j"); // Día del mes actual sin ceros
    $mes = date("n"); // Mes actual sin ceros
    $año = date("Y"); // Año actual en 4 dígitos
    $fechaActual = date("d/m/Y"); // Otener la fecha actual
    $primerDia = date("N", mktime(0,0,0,$mes,1,$año)); //Obtener el primer día de la semana
    $ultimoDia = date("d", mktime(0,0,0, $mes + 1, 0, $año)); //Obtener el último día del mes
    $totalDias = date("t"); // El total de días del mes actual
    $meses = array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre');
    $calendario = $primerDia+$ultimoDia; // Margen entre el primer día de la semama y el último del mes
    $dia = 0; // Los días que tendrá el calendario
?>

<!DOCTYPE html>
<head>
	<meta charset="utf-8";>
	<title>Calendario del mes</title>

	<style type="text/css">
		h1{
			text-align: center;
		}
		.fecha{
			text-align: right;
		}
		div{
			position: relative;
			width: 40%;
			margin: auto;
		}
		table{
			text-align: center;
		}
		.actual{
			border: 5px solid red;
		}
	</style>
</head>
<body>
	<div width: 50%>
		<h1>Calendario del mes</h1>
		<h2><?php echo $meses[$mes] ?></h2>
		<h3 class="fecha"><?php echo "Hoy es $fechaActual" ?></h3>

		<table border=2 cellpadding=10% style="margin: 0 auto">
			<tr>
				<th>Lunes</th><th>Martes</th><th>Miércoles</th><th>Jueves</th><th>Viernes</th><th>Sábado</th><th>Domingo</th>
			</tr>
			<tr>
				<?php
                for($i = 1;$i <= 35;$i++){ // Tamaño de la tabla. El número máximo es el total de celdas que ocuparía un mes que no empieza en un lunes

					if($i == $primerDia){

                        $dia = 1; // Colocar el primer día
                        
                    }
                    
                    if($i<$primerDia || $i>=$calendario){

                        echo "<td>&nbsp;</td>"; // Tapar los días sobrantes vaciando la celda 
        
                    }else{

                        if($dia == $diaActual){

                            echo "<td class='actual'>$dia</td>"; // Mostrar día actual

                        }else{

                            echo "<td>$dia</td>"; // Mostrar el resto de días

                        }

                        

                    }

                    $dia++;
                    
					if($i % 7 == 0){
						echo "</tr><tr>\n"; // Salto a la siguiente fila cuando se acaba la semana
                    }
                    
				}
				?>
			</tr>
		</table>
	</div>
</body>
</html>
